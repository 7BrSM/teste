fosforo = {}

function fosforo.registrar_nos()

-- Minrios

minetest.register_node("fosforo:rocha_com_fosforo", {
		description = "Minrio de fosforo",
		tiles = {"default_stone.png^fosforo_minerio.png"},
		groups = {cracky = 3},
		drop = "fosforo_cristal",
		sounds = default.node_sound_stone_defaults(),
		light_source = 5,
})

minetest.register_node("fosforo:bloco_de_fosforo", {
		description = "Bloco de fosforo",
		tiles = {"fosforo_bloco.png"},
		is_ground_content = false,
		groups = {cracky = 3},
		sounds = default.node_sound_stone_defaults(),
		light_source = 9,
})

end

-- Ores

function fosforo.colocar_nos_em_ambientes_que_usam_mgv7()

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "fosforo:rocha_com_fosforo",
		wherein        = "default:stone",
		clust_scarcity = 8 * 8 * 8,
		clust_num_ores = 9,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "fosforo:rocha_com_fosforo",
		wherein        = "default:stone",
		clust_scarcity = 8 * 8 * 8,
		clust_num_ores = 8,
		clust_size     = 3,
		y_max          = 64,
		y_min          = -31000,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "fosforo:rocha_com_fosforo",
		wherein        = "default:stone",
		clust_scarcity = 24 * 24 * 24,
		clust_num_ores = 27,
		clust_size     = 6,
		y_max          = 0,
		y_min          = -31000,
	})

end

-- Receitas

function fosforo.registrar_receitas()

minetest.register_craftitem("fosforo:fosforo_cristal",{
	description = "Cristal de Fosforo",
		inventory_image = "fosforo_cristal.png",
		groups = {coal = 1, flammable = 1}
	})

	minetest.register_craft({
		output = 'fosforo:fosforo_bloco',
		recipe = {
			{'fosforo_cristal', 'fosforo_cristal', 'fosforo_cristal'},
			{'fosforo_cristal', 'fosforo_cristal', 'fosforo_cristal'},
			{'fosforo_cristal', 'fosforo_cristal', 'fosforo_cristal'},
		}
	})

	minetest.register_craft({
		output = 'fosforo_cristal9',
		recipe = {
			{'fosforo:bloco_de_fosforo'},
		}
	})

	minetest.register_craft({
		output = 'default:torch 4',
		recipe = {
			{'fosforo_cristal'},
			{'group:stck'},
		}
	})

end

local mg_name = minetest.get_mapgen_setting("mg_name")

if mg_name == "v7" then
	fosforo.registrar_nos()
	fosforo.colocar_nos_em_ambientes_que_usam_mgv7()
	fosforo.registrar_receitas()
	minetest.register_alias("rocha_com_fosforo", "fosforo:rocha_com_fosforo")
	minetest.register_alias("fosforo_cristal", "fosforo:fosforo_cristal")
else
	minetest.log("warning", "[fosforo] Nenhum n registrado.")

end
